Template.emailView.onRendered(function() {
    // set the email isRead = true
});

Template.emailView.helpers({
    maildate: function() {
        return moment(this.date).format('DD.MM.YYYY');
    },
    mailsince: function() {
        var diffD = moment().diff(this.date, 'days');
        if (diffD == 0) {
            return moment(this.date).format('LTS').concat(" (", moment().diff(this.date, 'hours').toString() , " hours)" );
        }
        else {
            return moment(this.date).format('LTS').concat( " (", moment().diff(this.date, 'days').toString() , " days)");
        }
    },
    mailmessage: function() {
        var mailmessage = this.message;//.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll(/\n/, "&lt;br /&gt;");
        //$('#mailmessage').append( this.message );
        return mailmessage;
    }
});

Template.emailView.events({
    'click #reply': function(event) {
        Router.go("emailCompose", {_id: this._id});
    }
});