Template.emailCompose.onRendered(function() {
    console.log("email compose onrendered");
     // Initialize summernote plugin
    $('.summernote').summernote({
        toolbar: [
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            ['textsize', ['fontsize']],
            ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
        ]
    });
    Meteor.typeahead.inject();
});

Template.emailCompose.onCreated(function() {
    var self = this;
    self.subscribe('contacts');
});

Template.emailCompose.events({
    'click #sendMail': function(event) {
        
        var userToId = $('#userTo').data('id');
        
        var message = {
            to: $('#userTo').val(),
            userToId: userToId,
            subject: $('#subject').val(),
            message: $('div.note-editable[contenteditable="true"]').code(),
            type:'email'
        }
        Meteor.call('sendMessage', message, function(err, result) {
            if (result) {
                swal({ title: 'Some error occured',
                                text: error.reason,
                                allowEscapeKey:false,
                                closeOnCancel:false,
                                closeOnConfirm: true,
                                type:'error'
                            });
            }
            else{
                Router.go("mailbox");
            }
        });
    }
});

Template.emailCompose.helpers({
    users: function() {
        return Meteor.users.find({_id: {$ne: Meteor.userId()}}).fetch().map(function(userTo) {
            $('#userTo').data('id', userTo._id);
            return userTo.persodata.lname + ' ' +  userTo.persodata.fname;
        });
    },
    subject: function() {
        return this != null && this.subject != undefined ? "RE: " + this.subject : "";
    },
    mailTitle: function() {
        return this != null && this.subject != undefined ? "Reply message" : "New message";
    }
});