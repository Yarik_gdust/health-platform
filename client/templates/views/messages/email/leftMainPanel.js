Template.leftMailPanel.onRendered(function() {
    console.log('left panel ' + this.parent);
});

Template.leftMailPanel.helpers({
    nbInbox: function() {
        var usermessages = UserMessages.find({isSent:false}, {limit:50, sort: {date:-1}}).fetch();
        return usermessages.length;
    },
    nbSent: function() {
        var usermessages = UserMessages.find({isSent:true}, {limit:50, sort: {date:-1}}).fetch();
        return usermessages.length;
    },
    inboxClass: function() {
        return this.parent == "mailbox" ? "active" : "";
    },
    sentClass: function() {
        return this.parent == "mailsent" ? "active" : "";
    }
});