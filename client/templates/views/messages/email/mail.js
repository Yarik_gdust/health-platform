
Template.mail.helpers({
    status: function() {
        return this.isRead ? "" : "unread";
    },
    formatdate: function() {
        return moment(this.date).format("llll");
    }
});