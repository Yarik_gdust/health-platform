Template.mailbox.onRendered(function() {
     // testing
     var email = {
     "userId" : "vKhsrfZrvpgN9qrCT",
            "type" : "email",
            "date" : "2016-04-20",
            "from" : "Test Test",
            "to" : "Dr Toto",
            "subject" : "Test Subject",
            "message" : "This is a test message",
            "isRead" : false,
            "isDeleted" : false
    };
     //UserMessages.insert(email);
});

Template.mailbox.helpers({
    mails: function() {
        var usermessages = UserMessages.find({isSent:false}, {limit:50, sort: {date:-1}}).fetch();
        return usermessages;
    },
    sents: function() {
        var usermessages = UserMessages.find({isSent:true}, {limit:50, sort: {date:-1}}).fetch();
        return usermessages;
    },
    nbMessagesNotRead: function() {
        var usermessages = UserMessages.find({isSent:false, isRead:false}, {limit:50, sort: {date:-1}}).fetch();
        return usermessages.length;
    }
});

Template.mailbox.events({
    'click .table-mailbox tr': function(event, template) {
        event.preventDefault();
        var id = $(event.currentTarget).data('id');
        Router.go('emailView', {_id: id});
    }
});